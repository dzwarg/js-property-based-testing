# JS Property-Based Testing

This is a presentation about property based testing. To run, check out the
source and start a static HTTP server, or open the `index.html` document in your
favorite [evergreen browser](http://eisenbergeffect.bluespire.com/evergreen-browsers/).

[Big list of http static server one-liners](https://gist.github.com/willurd/5720255)

## Author

 * [David Zwarg](mailto:dzwarg@massmutual.com)

## Powered By impress.js

[impress.js demo](http://impress.github.io/impress.js/) by [@bartaz](http://twitter.com/bartaz)

It's a presentation framework based on the power of CSS3 transforms and
transitions in modern browsers and inspired by the idea behind prezi.com.
