'use strict'

import jsc from 'jsverify'

describe('Combinations of arbitraries', () => {
    describe('using "either"', () => {
        jsc.property('is one or the other', 'either bool nat', x => {
            return (typeof x.value == 'number' && x.value >= 0) ||
                (typeof x.value == 'boolean')
        })
    })

    describe('using "pair"', () => {
        jsc.property('is both types at once', 'pair nestring uint16', x => {
            return x[0].length >= 0 &&
                x[1] >= 0 && x[1] < 65536
        })
    })

    describe('using "sum"', () => {
        jsc.property('is any type in the sum', 'bool | int8', x => {
            return (x.idx == 0 && typeof x.value == 'boolean') ||
                (x.idx == 1 && typeof x.value == 'number' && x.value >= -128 && x.value < 128)
        })
    })

    describe('using "dict"', () => {
        jsc.property('is an object of arbitrary shape', 'dict nat', x => {
            return Object.keys(x).reduce((acc, key) => {
                return acc && typeof x[key] === 'number'
            }, true)
        })
    })

    describe('using "array"', () => {
        jsc.property('is an array of arbitrary length', 'array string', x => {
            return x.reduce((acc, item) => {
                return acc && typeof item === 'string'
            }, true)
        })
    })

    describe('using "oneof"', () => {
        jsc.property('is one of the listed arbitraries', jsc.oneof(jsc.bool, jsc.nat, jsc.nestring), x => {
            return (typeof x === 'boolean') ||
                (typeof x === 'number' && x >= 0) ||
                (typeof x === 'string' && x.length > 0)
        })
    })
})
