'use strict'

import jsc from 'jsverify'

describe('Custom arbitraries', () => {
    const formattedNumber = jsc.bless({
        // generate a number between 1,000 and 179,769,313,486,232,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000
        generator: jsc.number(1000, Number.MAX_VALUE / 1000000).generator.map(x => x.toLocaleString())
    })

    jsc.property('mapping from an existing generator', formattedNumber, x => {
        const commas = x.match(/,/g)

        return commas.length > 0
    })

    const recordArbitrary = jsc.record({
        index: 'nat',
        name: 'asciinestring',
        relations: 'array uint8'
    })

    jsc.property('specific shapes of records', recordArbitrary, x => {
        return x.index >= 0 &&
            x.name.length > 0 &&
            x.relations.reduce((acc, item) => {
                return acc && item >= 0 && item < 256
            }, true)
    })

    jsc.property('complex compound arbitrary', '[(either bool nat) | (pair (dict nat) (array uint8))]', x => {
        if (typeof x == 'object' &&
            x.hasOwnProperty('length') &&
            x.length == 0) return true

        const leftBool = item => {
            return item.idx == 0 && typeof item.value.value === 'boolean'
        }

        const leftNat = item => {
            return item.idx == 0 && typeof item.value.value === 'number' && item.value.value >= 0
        }

        const rightPairDict = item => {
            return typeof item.value[0] === 'object' &&
                Object.keys(item.value[0]).reduce((acc, dictItem) => {
                    return acc &&
                        typeof item.value[0][dictItem] === 'number' &&
                        item.value[0][dictItem] >= 0
                }, true)
        }

        const rightPairArray = item => {
            return typeof item.value[1] === 'object' &&
                item.value[1].reduce((acc, arrItem) => {
                    return acc &&
                        typeof arrItem === 'number' &&
                        arrItem >= 0 &&
                        arrItem < 256
                }, true)
        }

        const rightPair = item => {
            return item.idx == 1 && typeof item.value === 'object' &&
                item.value.hasOwnProperty('length') &&
                rightPairDict(item) &&
                rightPairArray(item)
        }

        const validItems = x.reduce((acc, item) => {
            // console.log(item)
            return leftBool(item) || leftNat(item) || rightPair(item)
        }, true)

        return validItems
    })
})
