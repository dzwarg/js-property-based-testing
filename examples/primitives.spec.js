'use strict'

import jsc from 'jsverify'

describe('Primitive arbitraries', () => {
    describe('as natural numbers', () => {
        jsc.property('generates natural numbers via DSL', 'nat', x => {
            return x >= 0
        })

        jsc.property('generates natural numbers via API', jsc.nat, x => {
            return x >= 0
        })
    })

    describe('as integers', () => {
        jsc.property('generates whole numbers only', 'integer', x => {
            const y = parseInt(x, 10)

            return x - y === 0
        })
    })

    describe('as boolean', () => {
        jsc.property('double negative boolean test', 'bool', x => {
            return x === !!x
        })
    })

    describe('as string', () => {
        jsc.property('which may be empty', 'string', x => {
            return x.length >= 0
        })

        jsc.property('which may be not empty', 'nestring', x => {
            return x.length >= 1
        })

        jsc.property('which can be ASCII characters only', 'asciinestring', x => {
            return /^[\x00-\x7F]*$/.test(x)
        })
    })
})
